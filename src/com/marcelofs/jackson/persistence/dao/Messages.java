package com.marcelofs.jackson.persistence.dao;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;

import com.marcelofs.jackson.persistence.data.Message;

public class Messages extends GenericDAO<Message> {

	public Messages(PersistenceManager pm) {
		super(Message.class, pm);
	}

	public Message get(long id) {
		try {
			return super.get(id);
		} catch (JDOObjectNotFoundException e) {
			return add(new Message().setKey(toKey(id)));
		}
	}
}